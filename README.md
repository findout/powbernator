# Powbernator

Ubuntu 21.10 didn't Hibernate on power low. This app helps by doing Hibernate when
power is below PercentageAction value, in /etc/UPower/UPower.conf.

## Installation

Install it:

    sudo cp powbernator.service /etc/systemd/system

Start it:

    sudo systemctl start powbernator

Check that it started successfully:

    sudo systemctl status powbernator

You should find the text "active (running)" somewhere in what is printed.

Make it start automatically at boot:

    sudo systemctl enable powbernator
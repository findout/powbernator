import time
import configparser
import subprocess

# read bat related file and return as string
def read_bat(sub_path):
    with open('/sys/class/power_supply/BAT0/' + sub_path, 'r') as f:
        return f.read().rstrip()

# read config file to get the battery charging level threshold value for hibernation
config = configparser.ConfigParser()
config.read("/etc/UPower/UPower.conf")
if not 'UPower' in config:
    raise RuntimeError('Invalid config - missing section UPower')
percentage_action=int(config['UPower']['PercentageAction'])

# read battery charging level periodically and hibernate if lower than threshold value
while True:
    if read_bat('status') == 'Discharging':
        if int(read_bat('capacity')) <= percentage_action:
            subprocess.run(["systemctl", "hibernate"])
    time.sleep(15)
